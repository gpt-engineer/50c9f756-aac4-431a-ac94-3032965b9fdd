document.addEventListener('DOMContentLoaded', () => {
  const emojis = ['😀', '😂', '👍', '💻', '🎨', '🚀'];
  const body = document.body;

  function createEmoji() {
    const emoji = document.createElement('div');
    emoji.classList.add('absolute', 'opacity-50');
    emoji.style.fontSize = Math.random() * 20 + 10 + 'px';
    emoji.style.left = Math.random() * 100 + 'vw';
    emoji.style.top = Math.random() * 100 + 'vh';
    emoji.style.transform = `translate(-50%, -50%)`;
    emoji.innerText = emojis[Math.floor(Math.random() * emojis.length)];
    body.appendChild(emoji);

    setTimeout(() => {
      emoji.remove();
    }, 4000);
  }

  setInterval(createEmoji, 300);
});
